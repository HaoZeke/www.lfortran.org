---
headless: true
date: 2021-04-17
---

LFortran is a modern open-source (BSD licensed) interactive Fortran compiler
built on top of LLVM. It can execute user's code interactively to allow
exploratory work (much like Python, MATLAB or Julia) as well as compile to
binaries with the goal to run user's code on modern architectures such as
multi-core CPUs and GPUs.

LFortran is in development (pre-alpha stage), we are working towards releasing
the Minimum Viable Product (MVP) at the end of the summer 2021, progress
towards MVP: [#313](https://gitlab.com/lfortran/lfortran/-/issues/313). We are
looking for contributors, if you are interested, please get in touch with us,
we will be happy to get you up to speed.

Main repository at GitLab:
[https://gitlab.com/lfortran/lfortran](https://gitlab.com/lfortran/lfortran)
[![GitLab Stars](https://img.shields.io/badge/dynamic/json?color=white&label=Stars&query=%24.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F4494718)](https://gitlab.com/lfortran/lfortran/-/starrers)  
GitHub mirror:
[https://github.com/lfortran/lfortran](https://github.com/lfortran/lfortran)
{{< github_lfortran_button >}}  
Twitter: [@lfortranorg](https://twitter.com/lfortranorg)  
Any questions? Ask us on Zulip [![project chat](https://img.shields.io/badge/zulip-join_chat-brightgreen.svg)](https://lfortran.zulipchat.com/)
or our [mailing list](https://groups.io/g/lfortran). You can also use the
Fortran Discourse [forum](https://fortran-lang.discourse.group).

**Google Summer of Code 2021**: LFortran is participating in GSoC 2021 and we have three students, you can follow their progress here:

* [Thirumalai Shaktivel](https://gitlab.com/Thirumalai-Shaktivel)
  ([@sh0ck_thi](https://twitter.com/sh0ck_thi)): weekly
  [reports](https://fortran-lang.discourse.group/t/lfortran-gsoc-student-progress-thirumalai-shaktivel-ast-project/1271)
* [Gagandeep Singh](https://gitlab.com/czgdp18071)
  ([@czgdp1807](https://twitter.com/czgdp1807)): weekly
  [reports](https://fortran-lang.discourse.group/t/weekly-blog-posts-for-supporting-arrays-and-allocatables-in-lfortran/1277)
* [Rohit Goswami](https://gitlab.com/HaoZeke)
  ([@rg0swami](https://twitter.com/rg0swami)): weekly
  [reports](https://fortran-lang.discourse.group/t/gsoc-weekly-lfortran-and-computational-chemistry/1382)
